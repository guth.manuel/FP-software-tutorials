# FP-software-tutorials

This repository is meant as a short software guide for the FP. You can find ROOT examples as well as instructions to install ROOT on your machine.

## ROOT Tutorial
One exemplary Jupyter Notebook for the Hanle experiment is located [here](http://nbviewer.jupyter.org/urls/gitlab.com/guth.manuel/FP-software-tutorials/raw/master/Hanle_ROOT_fit_template.ipynb)

You can also find an adapted version of this script for the new oscilloscope in this repository (Example_new_osci.ipyn).


## Python Fit Tutorial
You can find a brief python tutorial for fitting from Frank [here](https://gitlab.sauerburger.com/frank/FP-python-examples).

## Recommended Packages

    * Numpy 
    * Pandas
    * SciPy
    * ROOT
    * root_numpy


## Usage via Docker image

You can simply use the following Docker image to run the software
```
docker run -it -p 8888:8888 gitlab-registry.cern.ch/mguth/root-docker/root3-fp:latest bash
```
inside the docker image you can start the jupyter notebook via
```
jupyter notebook --ip 0.0.0.0 --no-browser --allow-root
```
or alternatively you can use jupyter-lab
```
jupyter-lab --ip 0.0.0.0 --no-browser --allow-root
```

in your browser you can then either copy the url which is displayed in the terminal or type
```
localhost:8888
```


## Using the script in the physics CIP Pool

First you need to install pandas via
```
python -m pip install pandas --user
```
this step has to be done only once.

To use a newer ROOT version source the following script
```
source /opt/root_v6.06.08/bin/thisroot.sh
```
if you don't want to type this each time you can add the above line into your ```~/.bashrc```.

You can then open the script using
```
jupyter notebook
```

It might be necessary to install another package if you get amn error related to ```jinja2```, then type
```
pip install jinja2 --user
```
## Installation of pyROOT on macOS

The easiest way to install ROOT on macOS is to use homebrew. A good description for the homebrew installation can be found [here](http://osxdaily.com/2018/03/07/how-install-homebrew-mac-os/).
A short summary:

type in your terminal
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
to install homebrew. Before installing ROOT you need to install python via
```
brew install python
```
probably you need to set a path in your ```~/.bash_profile```, you can do that in your terminal via 
```
echo "# homebrew path" >>~/.bash_profile
echo "export PATH=/usr/local/bin:/usr/local/sbin:$PATH" >>~/.bash_profile
```
Now you can install ROOT 
```
brew install root
```
this might take a while.
Finally you have to set another PATH in your ```~/.bash_profile```
```
echo "source /usr/local/bin/thisroot.sh" >>~/.bash_profile
```
Open a new terminal and start python 
```
python3
```
try the command
```
import ROOT
```
if this works you successfully installed pyROOT.
If not, check if python3 links to the correct python version from homebrew.

Any further packages can be installed using ```pip3```.

## Installation of pyROOT on Linux

You can download ROOT on the CERN [webpage](https://root.cern.ch/downloading-root). The best is to use the current PRO version and download the binary compatible to your OS (Ubuntu etc.). Check if your gcc version matches the one from ROOT.  

After unpacking the binaries via
```
gunzip root_*.tar.gz
tar xvf root_*.tar
```

you have to source the file ```thisroot.sh``` it is located in the folder ```bin```. To automate this you can also add it to your .bashrc via
```
echo "source <ROOTPATH>/bin/thisroot.sh" >>~/.bashrc
```
Please adapt your ROOT path.


## Installation of pyROOT on Windows
There are ROOT versions for Windows but I would not recommend them. The easiest way is to use the ubuntu bash shell under Windows (can be enabled in the developer mode).

The further installation of ROOT is then identical to the installtion under LINUX.